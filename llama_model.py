import torch
import torch.nn as nn

from utils import find_layers
from quant import make_quant

import transformers, accelerate
from transformers import LlamaConfig, LlamaForCausalLM, LlamaTokenizer

def build_model(model_name_or_path, bits, weights):

    with accelerate.init_empty_weights():
        config = LlamaConfig.from_pretrained(model_name_or_path)
        torch.set_default_dtype(torch.half)
        transformers.modeling_utils._init_weights = False
        torch.set_default_dtype(torch.half)
        model = LlamaForCausalLM(config)
        torch.set_default_dtype(torch.float)
        model = model.eval()
        layers = find_layers(model)
        for name in ['lm_head']:
            if name in layers:
                del layers[name]
        make_quant(model, layers, bits)
        model = accelerate.load_checkpoint_and_dispatch(
            model=model, checkpoint=weights, device_map="auto", no_split_module_classes=["LlamaDecoderLayer"]
        )
    model.seqlen = 2048

    return model

def build_model_offload(model_name_or_path, bits, weights, max_memory=None, is_v1_model=False):
    import accelerate
    from transformers import LlamaConfig, LlamaForCausalLM, LlamaTokenizer

#    if max_memory is None:
    max_memory = {0: '1Gib',  1: '16Gib', 2: '16Gib', 'cpu': '200Gib'}

    with accelerate.init_empty_weights():
        config = LlamaConfig.from_pretrained(model_name_or_path)
        model = LlamaForCausalLM(config)
        model = model.eval()
        layers = find_layers(model)
        for name in ['lm_head']:
            if name in layers:
                del layers[name]
        make_quant(model, layers, bits)
    accelerate.load_checkpoint_in_model(model, checkpoint=weights, device_map={'': 'cpu'})

    # rotary_emb fix
    for n, m in model.named_modules():
        if 'rotary_emb' in n:
            cos_cached = m.cos_cached.clone().cpu()
            sin_cached = m.sin_cached.clone().cpu()
            break

    model.seqlen = 2048

    print(max_memory)

    device_map = accelerate.infer_auto_device_map(model, max_memory=max_memory, no_split_module_classes=["LlamaDecoderLayer"])
#    model = accelerate.dispatch_model(model, device_map=device_map, offload_buffers=True, main_device=0)
    torch.cuda.empty_cache()

    # rotary_emb fix
    for n, m in model.named_modules():
        if 'rotary_emb' in n:
            if getattr(m, '_hf_hook', None):
                if isinstance(m._hf_hook, accelerate.hooks.SequentialHook):
                    hooks = m._hf_hook.hooks
                else:
                    hooks = [m._hf_hook]
                for hook in hooks:
                    if hook.offload:
                        if n + '.sin_cached' not in hook.weights_map.dataset.state_dict.keys():
                            hook.weights_map.dataset.state_dict[n + '.sin_cached'] = sin_cached.clone().cpu()
                            hook.weights_map.dataset.state_dict[n + '.cos_cached'] = cos_cached.clone().cpu()

    return model

