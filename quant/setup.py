from setuptools import setup, Extension
from torch.utils import cpp_extension

setup(
    name='quant',
    version='1.0.0',
    packages=["quant"],
    ext_modules=[cpp_extension.CUDAExtension(
        'quant_cuda', ['quant/cuda/quant_cuda.cpp', 'quant/cuda/quant_cuda_kernel.cu'],
        extra_compile_args={'nvcc': ['-O3']}
    )],
    cmdclass={'build_ext': cpp_extension.BuildExtension},
)
