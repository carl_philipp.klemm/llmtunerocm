#!/bin/sh

BASE_DIR=$(dirname "$0")

source $BASE_DIR/venv/bin/activate

python3 run_clm.py \
	--model_name_or_path "$BASE_DIR/data/llama-13b-hf" \
	--dataset_name wikitext \
	--dataset_config_name wikitext-2-raw-v1 \
	--weights=$BASE_DIR/data/llama-13b-4bit.pt \
	--do_train \
	--per_device_train_batch_size 2 \
	--per_device_eval_batch_size 8 \
	--gradient_accumulation_steps 2 \
	--num_train_epochs 1 \
	--logging_dir $BASE_DIR/log --logging_strategy=steps --save_steps=300 --save_strategy=steps \
	--output_dir $BASE_DIR/outputs/llama13B-wikitext\
	--preprocessing_num_workers=32 \
	--block_size 256
